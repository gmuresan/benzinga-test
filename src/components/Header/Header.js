import React from 'react'
import { connect } from 'react-redux'
import './Header.scss'
import { fetchStock } from 'store/portfolio'

class Header extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      symbol: ""
    };

    this.symbolChanged = this.symbolChanged.bind(this);
    this.fetchStock = this.fetchStock.bind(this);
    this._handleKeyPress = this._handleKeyPress.bind(this);
  }

  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.fetchStock();
    }
  }

  render() {
    return (
      <div className="header">
        <h1>Simple Stock Exchange</h1>
        <div className="lookup">
          <input type="text" onKeyPress={this._handleKeyPress} onChange={this.symbolChanged} placeholder="Enter Symbol" />
          <button type="button" onClick={this.fetchStock}>Lookup</button>
        </div>
      </div>
    );
  }

  fetchStock() {
    this.props.fetchStock(this.state.symbol);
  }

  symbolChanged(evt, a) {
    const value = evt.target.value;
    this.setState({symbol: value});
  }
}

const mapDispatchToProps = {
  fetchStock: fetchStock
}

const mapStateToProps = (state) => ({
  currentSymbol: state.portfolio.currentSymbol
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)

