import fetch from 'fetch-jsonp'

// ------------------------------------
// Constants
// ------------------------------------
export const LOOKUP_STOCK = 'LOOKUP_STOCK'
export const RECEIVE_LOOKUP_STOCK = 'RECEIVE_LOOKUP_STOCK'
export const BUY_STOCK = 'BUY_STOCK'
export const SELL_STOCK = 'SELL_STOCK'

// ------------------------------------
// Actions
// ------------------------------------
export function lookupStock(symbol = '') {
  return {
    type    : LOOKUP_STOCK,
    symbol : symbol
  }
}

function receiveStockLookup(json) {
  return {
    type: RECEIVE_LOOKUP_STOCK,
    json
  }
}

function buyStockAction(symbol, amount, price) {
  return {
    type: BUY_STOCK,
    symbol: symbol,
    amount: amount,
    price: price
  };
}
function sellStockAction(symbol, amount, price) {
  return {
    type: SELL_STOCK,
    symbol: symbol,
    amount: amount,
    price: price
  };
}

function getStockData(symbol) {
  return fetch(`https://data.benzinga.com/rest/richquoteDelayed?symbols=${symbol}`, { mode: 'no-cors' })
    .then(function(response) {
      return response.json();
    }).then(function(json) {
      return json[symbol];
    });
}

// ------------------------------------
// Specialized Action Creator
// ------------------------------------
export function fetchStock(symbol) {
  return dispatch => {
    dispatch(lookupStock(symbol));
    return getStockData(symbol)
      .then(json => dispatch(receiveStockLookup(json)))
      .catch(function(ex) {
        console.log(ex);
      });
  }
}

export function buyStock(symbol, amount) {
  return dispatch => {
    return getStockData(symbol).then(function(json) {
      dispatch(buyStockAction(symbol, amount, json.askPrice));
    });
  }
}

export function sellStock(symbol, amount) {
  return dispatch => {
    return getStockData(symbol).then(function(json) {
      dispatch(sellStockAction(symbol, amount, json.bidPrice));
    });
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  currentSymbol: null,
  currentStock: null,
  balance: 100000.0,
  stocks: {}
};

export default function portfolioReducer(state = initialState, action) {
  switch(action.type) {
    case LOOKUP_STOCK:
      return Object.assign({}, state, { currentSymbol: action.symbol });
      break;
    case RECEIVE_LOOKUP_STOCK:
      return Object.assign({}, state, { currentStock: action.json });
      break;
    case BUY_STOCK:
      const cost = action.amount * action.price;
      if(cost > state.balance) return state;
      let stock;
      if(!state.stocks[action.symbol]) {
        state.stocks[action.symbol] = {amount: action.amount, pricePaid: cost};
        stock = state.stocks[action.symbol];
      } else {
        stock = state.stocks[action.symbol];
        const newAmount = stock.amount + action.amount;
        stock.amount = newAmount;
        stock.pricePaid += cost;
      }

      stock.pricePaid = Math.round(stock.pricePaid * 100) / 100;
      state.balance -= cost;
      state.balance = Math.round(state.balance * 100) / 100;
      return Object.assign({}, state);
      break;

    case SELL_STOCK:
      stock = state.stocks[action.symbol];
      if(stock.amount <= 0) return state;
      if(stock.amount < action.amount) action.amount = stock.amount;
      const totalPrice = action.amount * action.price;
      const newAmount = stock.amount - action.amount;

      stock.amount = newAmount;
      stock.pricePaid -= totalPrice;
      stock.pricePaid = Math.round(stock.pricePaid * 100) / 100;

      state.balance += totalPrice;
      state.balance = Math.round(state.balance * 100) / 100;
      return Object.assign({}, state);
      break;

    default:
      return state;
  }
}

