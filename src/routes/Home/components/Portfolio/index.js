import React from 'react'
import { Row, Col, Table } from 'react-bootstrap'
import './Portfolio.scss'
const PropTypes = React.PropTypes;

class Portfolio extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  fetchStock(symbol) {
    this.props.fetchStock(symbol);
  }

  render() {
    const stocks = this.props.stocks;
    const fetchStock = this.fetchStock;
    const thiz = this;

    return (
      <div>
        <h4>Current Portfolio</h4>
        <h4>Cash: ${this.props.balance}</h4>

        <Table className="portfolio_table" responsive>
          <thead>
            <tr>
              <th>
                Company
              </th>
              <th>
                Quantity
              </th>
              <th>
                Price Paid
              </th>
              <th>
              </th>
            </tr>
          </thead>
          <tbody>
            {Object.keys(stocks).map(function(symbol) {
              return (
                <tr key={symbol}>
                  <td>
                    {symbol}
                  </td>
                  <td>
                    {stocks[symbol].amount}
                  </td>
                  <td>
                    {stocks[symbol].pricePaid}
                  </td>
                  <td>
                    <button onClick={fetchStock.bind(thiz, symbol)} type="button">View Stock</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>

      </div>
    );
  }
}

Portfolio.propTypes = {
  stocks: PropTypes.object.isRequired,
  fetchStock: PropTypes.func.isRequired
};

export default Portfolio

