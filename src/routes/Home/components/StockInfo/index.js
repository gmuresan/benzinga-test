import React from 'react'
import { Row, Col } from 'react-bootstrap'
import './StockInfo.scss'

class StockInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {amount: 0};

    this.amountChanged = this.amountChanged.bind(this);
    this.buyStock = this.buyStock.bind(this);
    this.sellStock = this.sellStock.bind(this);
  }

  render() {
    return (
      <div>
        <h4 className="stockInfoHeader">{this.props.currentStock.name} ({this.props.currentStock.symbol})</h4>

        <Row className="stockInfo">
          <Col md={6} className="stockInfoInner">
            <span>Bid</span>
            <div>
              {this.props.currentStock.bidPrice}
            </div>
          </Col>
          <Col md={6} className="stockInfoInner">
            <span>Ask</span>
            <div>
              {this.props.currentStock.askPrice}
            </div>
          </Col>
        </Row>

        <Row>
            <input onChange={this.amountChanged} type="number" placeholder="Quantity" />
            <button onClick={this.buyStock} type="button">Buy</button>
            <button onClick={this.sellStock} type="button">Sell</button>
        </Row>

      </div>
    );
  }

  amountChanged(event) {
    const amount = parseInt(event.target.value);
    this.setState({amount: amount});
  }

  buyStock() {
    this.props.buyStock(this.props.currentStock.symbol, this.state.amount);
  }

  sellStock() {
    this.props.sellStock(this.props.currentStock.symbol, this.state.amount);
  }
}

export default StockInfo

