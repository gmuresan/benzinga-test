import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import StockInfo from './StockInfo'
import Portfolio from './Portfolio'
import { Row, Col } from 'react-bootstrap'

class HomeView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let currentStockInfo = null;
    if(this.props.currentStock) {
      currentStockInfo = (
        <StockInfo
          buyStock={this.props.buyStock}
          sellStock={this.props.sellStock}
          currentStock={this.props.currentStock}
          fetchStock={this.props.fetchStock} />
      );
    }

    return (
      <div>
        <Row>
          <Col mdOffset={1} md={4}>
            {currentStockInfo}
          </Col>
          <Col mdOffset={1} md={5}>
            <Portfolio
              fetchStock={this.props.fetchStock}
              stocks={this.props.stocks}
              balance={this.props.balance} />
          </Col>
        </Row>
      </div>
    );
  }
}

HomeView.propTypes = {
  currentStock: React.PropTypes.object,
  fetchStock: React.PropTypes.func.isRequired,
  buyStock: React.PropTypes.func.isRequired,
  sellStock: React.PropTypes.func.isRequired,
  stocks: React.PropTypes.object.isRequired,
  balance: React.PropTypes.number.isRequired
};

export default HomeView;

